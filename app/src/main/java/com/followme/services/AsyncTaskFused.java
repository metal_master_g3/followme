package com.followme.services;

import android.app.Activity;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by root on 26/05/17.
 */

public class AsyncTaskFused extends AsyncTask<Activity, Void, Boolean> {
    public static ServiceGps gps;

    @Override
    protected Boolean doInBackground(Activity... params) {
        gps=new ServiceGps(params[0],params[0].getApplicationContext(),1);
        gps.connect();
        return gps.isConected();
    }

    public Boolean reconnect(){
        if(gps!=null) {
            gps.onConnected(null);
            return ServiceGps.isActiveGps;
        }else{
            return false;
        }
    }

    public void disconect(){
        if(gps!=null) {
            gps.disconnect();
        }
    }

    public boolean activarGeolocalizacion(Activity activity,Integer codeUi){
        if(gps!=null){
            return gps.activarGeolocalizacion(activity,codeUi);
        }else{
            return false;
        }
    }

}
