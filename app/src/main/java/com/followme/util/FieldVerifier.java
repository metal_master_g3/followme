package com.followme.util;

/**
 * Created by root on 26/05/17.
 */

import java.text.DecimalFormat;
import java.text.Normalizer;

/**
 * Created by root on 11/09/16.
 */
public class FieldVerifier {

    public static boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email)
                .matches();
    }

    public static boolean isEmpty(CharSequence value){
        return value.length()==0;
    }

    public static String cleanStringUpper(String texto) {
        texto = Normalizer.normalize(texto, Normalizer.Form.NFD);
        texto = texto.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return texto.toUpperCase();
    }

    public static String formatoDistanciaTuristaADestino(Double distanciaCalculada){
        String unidadMedida=" Kms";
        if(distanciaCalculada<1.0){
            unidadMedida=" Mts";
            distanciaCalculada=distanciaCalculada*1000;
        }
        DecimalFormat beanFormato=new DecimalFormat("0.0");
        String formatDistancia=beanFormato.format(distanciaCalculada);

        return formatDistancia.concat(unidadMedida);
    }
}

