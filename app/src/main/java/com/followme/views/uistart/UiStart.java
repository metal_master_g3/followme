package com.followme.views.uistart;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import android.app.Activity;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.followme.R;
import com.followme.model.beans.GeoPunto;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

/**
 * Created by root on 26/05/17.
 */

public class UiStart extends AppCompatActivity implements InterUiStart,GoogleMap.OnMyLocationButtonClickListener,GoogleMap.OnMyLocationChangeListener,
        OnMapReadyCallback,
        ActivityCompat.OnRequestPermissionsResultCallback{
    public static TextView txtGeoGps;
    public static TextView txtGeoFused;
    public static TextView txtGeoMap;
    protected static GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.ui_start);
        txtGeoGps=(TextView)this.findViewById(R.id.txtGeoGps);
        txtGeoFused=(TextView)this.findViewById(R.id.txtGeoFused);
        txtGeoMap=(TextView)this.findViewById(R.id.txtGeoMap);
        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        Log.d("onCreate","onCreate");
    }


    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    @Override
    public void onMyLocationChange(Location location) {

    }
}
