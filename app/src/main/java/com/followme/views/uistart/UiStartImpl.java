package com.followme.views.uistart;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.followme.model.beans.GeoPunto;
import com.followme.services.AsyncTaskFused;
import com.followme.services.ServiceGeolocalizacion;
import com.followme.services.ServiceGps;
import com.followme.util.GlobalConstants;
import com.followme.util.PermissionUtils;
import com.followme.util.UtilGeolocalizacion;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by root on 26/05/17.
 */

public class UiStartImpl extends UiStart {
    //public static GeoPunto geoGps;
    //public static GeoPunto geoFused;
    public static Boolean value = true;
    public static String textNewGeoFused="";
    public static String textNewGeoGps="";
    public static String textNewGeoMap="";
    public static String textOldFused;
    public static String textOldGps;
    public static String textOldMap;
    public static AsyncTaskFused asyncTaskFused;
    public static ServiceGeolocalizacion asyncTaskService;
    public static NewGeoPuntoAsync newGeo;
    private static String[] PERMISSIONSGPS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    private static final int REQUEST_GPS = 123;
    public int CODE_ACTIVE_GEO = 1000;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CODE_ACTIVE_GEO) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(getApplicationContext(), "Permisos otorgados", Toast.LENGTH_SHORT).show();
                asyncTaskFused.reconnect();
                asyncTaskService.onConnected(null);
            } else {
                asyncTaskFused.activarGeolocalizacion(this, CODE_ACTIVE_GEO);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("onStart", "onStart");
    }

    @Override
    public void onRestart() {
        super.onRestart();
        Log.d("onRestart", "onRestart");
    }

    @Override
    public void onResume() {
        super.onResume();
        asyncTaskService = new ServiceGeolocalizacion(this.getApplicationContext(), 1);
        asyncTaskService.onConnected(null);
        asyncTaskFused = new AsyncTaskFused();
        asyncTaskFused.execute(this);
        value = true;
        newGeo = new NewGeoPuntoAsync();
        newGeo.execute();
        activarGps();
        Log.d("onResume", "onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("onPause", "onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        value = false;
        Log.d("onStop", "onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Nuestro código a ejecutar en este momento
        asyncTaskService.disconnect();
        asyncTaskFused.disconect();
        asyncTaskFused.cancel(true);
        Log.d("onDestroy", "onDestroy");
    }

    public class NewGeoPuntoAsync extends AsyncTask<Void, Boolean, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            while (value) {
                //returnGeoPunto();
                /*if (geoGps != null) {
                    textNewGeoGps = (geoGps.getLatitude() == null ? "x" : geoGps.getLatitude().toString()) + ":" + (geoGps.getLongitude() == null ? "x" : geoGps.getLongitude().toString());
                    Log.d("geopuntoGps", textNewGeoGps);
                }
                if (geoFused != null) {
                    textNewGeoFused = (geoFused.getLatitude() == null ? "x" : geoFused.getLatitude().toString()) + ":" + (geoFused.getLongitude() == null ? "x" : geoFused.getLongitude().toString());
                    Log.d("geopuntoFused", textNewGeoFused);
                }*/

                if (GlobalConstants.currentLocationGps != null) {
                    textNewGeoGps = GlobalConstants.currentLocationGps.getLatitude() + ":" + GlobalConstants.currentLocationGps.getLongitude();
                    Log.d("geopuntoGps", textNewGeoGps);
                    System.out.println("geopuntoGps"+textNewGeoGps);
                }
                if (GlobalConstants.currentLocationFused != null) {
                    textNewGeoFused = GlobalConstants.currentLocationFused.getLatitude()+ ":" +GlobalConstants.currentLocationFused.getLongitude();
                    Log.d("geopuntoFused", textNewGeoFused);
                }
                publishProgress(true);
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Boolean... values) {
            //Log.d("onProgressUpdate", "onProgressUpdate");
            updateText();
        }

    }

    public void updateText() {
        if (!textNewGeoGps.equalsIgnoreCase(txtGeoGps.getText().toString()) && GlobalConstants.currentLocationGps!=null) {
            Log.d("onProgressUpdateENTRO", "onProgressUpdateENTRO");
            txtGeoGps.setText(textNewGeoGps);
            textOldGps = textNewGeoGps;
            mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                    .position(new LatLng(GlobalConstants.currentLocationGps.getLatitude(), GlobalConstants.currentLocationGps.getLongitude()))
                    .title("Gps"));
        }
        if (!textNewGeoFused.equalsIgnoreCase(txtGeoFused.getText().toString()) && GlobalConstants.currentLocationFused!=null) {
            Log.d("onProgressUpdateENTRO", "onProgressUpdateENTRO");
            txtGeoFused.setText(textNewGeoFused);
            textOldFused = textNewGeoFused;
            mMap.addMarker(new MarkerOptions()
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                    .position(new LatLng(GlobalConstants.currentLocationFused.getLatitude(), GlobalConstants.currentLocationFused.getLongitude()))
                    .title("Fused"));
        }
    }


    private void requestGeolocalizacionPermission() {
        ActivityCompat.requestPermissions(this, PERMISSIONSGPS, REQUEST_GPS);
    }


    /*public void returnGeoPunto() {
        if(ServiceGeolocalizacion.currentLocation!=null) {
            geoGps = ServiceGeolocalizacion.currentGeoPunto;
        }else{
            geoGps=null;
        }
        if(ServiceGps.currentLocation!=null) {
            geoFused = ServiceGps.currentGeoPunto;
        }else{
            geoFused=null;
        }
    }*/

    public void returnGeoPuntoMap(){
        Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        if(!ServiceGps.aceptoUsoGeolocalizacion) {
            asyncTaskFused.activarGeolocalizacion(this, CODE_ACTIVE_GEO);
        }
        if (mMap.isMyLocationEnabled()) {
            Location locMap=mMap.getMyLocation();
            if (locMap != null) {
                GlobalConstants.currentTimeLocationMap=(new java.util.Date()).getTime();
                GlobalConstants.currentLocationMap=locMap;
                if (GlobalConstants.currentLocationMap != null) {
                    textNewGeoMap = GlobalConstants.currentLocationMap.getLatitude() + ":" + GlobalConstants.currentLocationMap.getLongitude();
                    Log.d("geopuntoMap", textNewGeoMap);
                }
                if (!textNewGeoMap.equalsIgnoreCase(txtGeoMap.getText().toString()) && GlobalConstants.currentLocationMap != null) {
                    Log.d("onProgressUpdateENTRO", "onProgressUpdateENTRO");
                    txtGeoMap.setText(textNewGeoMap);
                    textOldMap = textNewGeoMap;
                    mMap.addMarker(new MarkerOptions()
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA))
                            .position(new LatLng(GlobalConstants.currentLocationMap.getLatitude(), GlobalConstants.currentLocationMap.getLongitude()))
                            .title("Map"));
                }
            }
        }
    }

    @Override
    public void onMyLocationChange(Location location) {
        //returnGeoPuntoMap();
    }

    @Override
    public boolean onMyLocationButtonClick() {
        returnGeoPuntoMap();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationChangeListener(this);
        enableMyLocation();
    }

    public void activarGps() {
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                ) {
            requestGeolocalizacionPermission();
        } else {
            try {
                if(!ServiceGps.aceptoUsoGeolocalizacion) {
                    asyncTaskFused.activarGeolocalizacion(this, CODE_ACTIVE_GEO);
                }
            } catch (Exception ex) {
                Toast.makeText(getApplicationContext(), "Habilite permisos manualmente", Toast.LENGTH_SHORT).show();
            }

        }
    }

    private void enableMyLocation() {
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                ) {
            // Permission to access the location is missing.
            /*PermissionUtils.requestPermission(this, REQUEST_GPS,
                    PERMISSIONS_GPS, true);*/
            requestGeolocalizacionPermission();

        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Toast.makeText(getApplicationContext(), "Habilite todos los permisos", Toast.LENGTH_SHORT).show();
        if (requestCode == REQUEST_GPS) {
            if (grantResults.length == 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    ) {
                if (asyncTaskFused != null) {
                    if (!asyncTaskFused.reconnect()) {
                        asyncTaskFused.activarGeolocalizacion(this, CODE_ACTIVE_GEO);
                    }
                }
                enableMyLocation();
            } else {
                Toast.makeText(getApplicationContext(), "Habilite todos los permisos", Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}
